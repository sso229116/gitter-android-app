package im.gitter.gitter.models;

import android.support.annotation.Nullable;

public class Repo {
    private final String uri;
    @Nullable
    private final String description;
    private final boolean isPrivate;

    public Repo(String uri, @Nullable String description, boolean isPrivate) {
        this.uri = uri;
        this.description = description;
        this.isPrivate = isPrivate;
    }

    public String getUri() {
        return uri;
    }

    public String getOwner() {
        return uri.split("/")[0];
    }

    public String getName() {
        return uri.split("/")[1];
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    @Override
    public String toString() {
        String text = getUri();
        if (isPrivate()) {
            // lock emoji
            text += " \uD83D\uDD12";
        }

        return text;
    }
}
