package im.gitter.gitter;

import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.toolbox.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import im.gitter.gitter.models.Room;
import im.gitter.gitter.models.RoomListItem;
import im.gitter.gitter.models.Suggestion;
import im.gitter.gitter.network.VolleySingleton;

public class RoomListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements PositionClickListener {

    private static final int SUGGESTED_ROOMS_THRESHOLD = 10;
    private static final int ROOM_VIEW_TYPE = 0;
    private static final int SEPARATOR_VIEW_TYPE = 1;
    private final ImageLoader imageLoader;
    private final Drawable mentionBadge;
    private final Drawable unreadBadge;
    private final Drawable activityBadge;
    private final int avatarSize;
    private final RoomListItemSelectListener selectListener;
    private Cursor cursor;
    private List<Suggestion> suggestedRooms = new ArrayList<>();

    public RoomListAdapter(Context context, RoomListItemSelectListener selectListener) {
        imageLoader = VolleySingleton.getInstance(context).getImageLoader();
        mentionBadge = ContextCompat.getDrawable(context, R.drawable.mention_badge);
        unreadBadge = ContextCompat.getDrawable(context, R.drawable.unread_badge);
        activityBadge = ContextCompat.getDrawable(context, R.drawable.activity_badge);
        avatarSize = context.getResources().getDimensionPixelSize(R.dimen.avatar_with_badge_avatar_size);
        this.selectListener = selectListener;
        setHasStableIds(true);
    }

    public int getItemViewType(int position) {
        return cursor.getCount() == position ? SEPARATOR_VIEW_TYPE : ROOM_VIEW_TYPE;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ROOM_VIEW_TYPE) {
            View roomListItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.avatar_with_badge, parent, false);
            return new RoomListItemViewHolder(roomListItemView, this);
        } else {
            View headingView = LayoutInflater.from(parent.getContext()).inflate(R.layout.room_list_heading, parent, false);
            return new HeadingViewHolder(headingView);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == ROOM_VIEW_TYPE) {
            RoomListItemViewHolder roomViewHolder = (RoomListItemViewHolder) holder;
            RoomListItem roomListItem;
            int cursorCount = cursor.getCount();
            if (position < cursorCount) {
                cursor.moveToPosition(position);
                roomListItem = Room.newInstance(cursor);
            } else {
                roomListItem = suggestedRooms.get(position - cursorCount - 1);
            }

            RoomListItemViewHolder.bind(roomViewHolder, roomListItem, imageLoader, avatarSize, mentionBadge, unreadBadge, activityBadge);
        }
    }

    @Override
    public int getItemCount() {
        int itemCount = cursor != null ? cursor.getCount() : 0;
        if (itemCount < SUGGESTED_ROOMS_THRESHOLD && suggestedRooms.size() > 0) {
            itemCount = itemCount + 1 + suggestedRooms.size();
        }
        return itemCount;
    }

    @Override
    public long getItemId(int position) {
        RoomListItem item = getRoomListItem(position);
        if (item != null) {
            return item.getUri().hashCode();
        } else {
            return RecyclerView.NO_ID;
        }
    }

    @Override
    public void onClick(int position) {
        RoomListItem item = getRoomListItem(position);
        if (item != null) {
            selectListener.onSelect(item);
        }
    }

    public void setCursor(Cursor cursor) {
        if (this.cursor != cursor) {
            this.cursor = cursor;
            notifyDataSetChanged();
        }
    }

    public void setSuggestedRooms(List<Suggestion> suggestedRooms) {
        if (!this.suggestedRooms.equals(suggestedRooms)) {
            this.suggestedRooms = suggestedRooms;
            notifyDataSetChanged();
        }
    }

    private RoomListItem getRoomListItem(int position) {
        if (position < cursor.getCount() && position >= 0) {
            cursor.moveToPosition(position);
            return Room.newInstance(cursor);
        } else if (position == cursor.getCount()) {
            // its the heading view so return null
            return null;
        } else if (position > cursor.getCount()) {
            return suggestedRooms.get(position - cursor.getCount() - 1);
        } else {
            return null;
        }
    }

    private class HeadingViewHolder extends RecyclerView.ViewHolder {

        public HeadingViewHolder(View itemView) {
            super(itemView);
        }
    }
}
