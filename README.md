# gitter-android-app

Its Gitter, but on Android!

Available on the Google Play Store: https://play.google.com/store/apps/details?id=im.gitter.gitter

| Room list                            | Chat view                            |
| ------------------------------------ | ------------------------------------ |
| ![](https://i.imgur.com/efHkXiH.png) | ![](https://i.imgur.com/rUrDgFC.png) |

## Early releases (internal, Alpha, Beta)

Here are the opt-in URLs to get early releases,

- Beta: https://play.google.com/apps/testing/im.gitter.gitter
- Alpha: https://play.google.com/apps/testing/im.gitter.gitter
  - You also have to be manually added to the alpha group but I haven't successfully installed an alpha app (just always shows beta)
- Internal testing: https://play.google.com/apps/internaltest/4700266577877401248
  - You also have to manually added to the internal test group (max 100 people)

After opting into a program, it's best to uninstall the current version of the app you have, wait a bit, then install again from the play store. You can check the version in the slide-out left menu after signing in or in the Android app settings area

## Install prerequisites

1.  Run `gradlew fetchWebappAssets` to retrieve the latest assets from the [`webapp`](https://gitlab.com/gitlab-org/gitter/webapp) project

    1. You can also build these assets locally if you prefer,
    1. Clone and setup the [`webapp`](https://gitlab.com/gitlab-org/gitter/webapp) project
       - At a very minimum, `npm install`. You don't necessarily need to run the webapp locally and can skip the Docker stuff
    1. In the [`webapp`](https://gitlab.com/gitlab-org/gitter/webapp) project, run `npm run build-android-assets`
    1. Symlink the webapp embedded build asset output to the Android project
       - macOS: `mkdir -p mkdir app/src/main/assets && ln -s /Users/<YOUR_USERNAME>/Documents/gitlab/webapp/output/android/www /Users/<YOUR_USERNAME>/Documents/gitlab/gitter-android-app/app/src/main/assets/www`
       - Windows: `(mkdir app\src\main\assets || true) && mklink /D "C:\Users\<YOUR_USERNAME>\Documents\GitLab\gitter-android-app\app\src\main\assets\www" "C:\Users\<YOUR_USERNAME>\Documents\GitLab\webapp\output\android\www"`

1.  Make a copy of `secrets.properties.example` named `secrets.properties` and follow the comment instructions inside
1.  Download the Android IDE: [Android Studio](http://developer.android.com/sdk/installing/studio.html) (you may need to install java by following the prompts)
    1. Optionally: Install the Android SDK: `brew install android-sdk`, Select the SDK that `brew` logged out back in the previous command
1.  Open this project with in Android Studio IDE
1.  The IDE will complain about "Gradle sync failed". Just do what it says.
1.  Once the IDE stops giving good suggestions go to **Tools** -> **Android** -> **SDK Manager** and do what the SDK Manager says.
1.  Once the SDK Manager stops giving good suggestions, use it to install the Google Repository and the Android Support Repository
1.  Your IDE should stop whining now.

## Using the emulator

### Setup

If you are using an Intel CPU, install the accelerator or emulation will be dog slow. If you are using Hyper-V on Windows, see the section below instead because Intel HAXM will refuse to install.

1.  Open the SDK Manager and download the Intel Emulator Accelerator, https://i.imgur.com/9viMhHQ.png https://i.imgur.com/Ds94V4a.png
1.  Go to your SDK_HOME and find the the correct executable that you just downloaded
    - macOS: `/usr/local/var/lib/android-sdk/extras/intel/Hardware_Accelerated_Execution_Manager/IntelHAXM_1.1.1_for_10_9_and_above.dmg`)
    - Windows: `C:\Users\<YOUR_USERNAME>\AppData\Local\Android\Sdk\extras\intel\Hardware_Accelerated_Execution_Manager\`
1.  Open the executable and install your new kernel extension

#### Emulator with Hyper-V installed on Windows 10

If you are using Hyper-V on Windows, follow the following steps derived from this post, https://blogs.msdn.microsoft.com/visualstudio/2018/05/08/hyper-v-android-emulator-support/

1.  Enable features **Hyper-V** and **Windows Hypervisor Platform**(you probably don't have this enabled already) in **Turn Windows features on or off** (requires a computer restart), https://i.imgur.com/99fnVrN.png
1.  Install Java Development Kit (JDK), http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
1.  Install Visual Studio Tools for Xamarin preview (15.8 Preview 1 or higher with Mobile development with .NET (Xamarin) workload), http://aka.ms/hyperv-emulator-dl
1.  In Visual Studio: [Tools -> Options -> Xamarin -> Android Settings](https://docs.microsoft.com/en-us/xamarin/android/troubleshooting/questions/android-sdk-location?tabs=vswin)
    - Android SDK location: `C:\Users\<YOUR_USRNAME>\AppData\Local\Android\Sdk`
1.  Create `C:\Users\<YOUR_USERNAME>\.android\advancedFeatures.ini` with the contents: `WindowsHypervisorPlatform = on`

### Running on a virtual device

2.  Open the AVD Manager (**Tools** -> **Android** -> **AVD Manager**), https://i.imgur.com/881HJHx.png
3.  Create a new Virtual Device. The size/model doesn't matter that much
    - Select a system image that has both a `x86_64` ABI and Google play services
4.  Finish and click play!

### Debugging the WebView

See https://developers.google.com/web/tools/chrome-devtools/remote-debugging/webviews

## Release

Releases are handled with [Fastlane](https://fastlane.tools/) via GitLab CI.

Make sure the last `webapp` build on `master` branch succeeded (necessary for getting the mobile assets artefact).

 First run the `publishInternal` job, test, then run the subsequent promotion jobs (alpha -> beta -> production).

#### Bumping the version

- `versionCode` is automatically set to the CI pipeline IID (always increments)
- `versionName` includes a semver like number along with the commit sha the build was from
  - You can manually update the semver version in [`app/build.gradle`](https://gitlab.com/gitlab-org/gitter/gitter-android-app/blob/master/app/build.gradle)

#### Updating metadata

Meta data is stored in `fastlane/metadata/android`

- `title`
- `short_description`
- `full_description`
- `changelogs/<versionCode>.txt`
  - Just change `changelogs/CURRENT_VERSION.txt` and the CI will handle adding the correct `versionCode`

#### Signing

> When using App Signing by Google Play, you will use two keys: the app signing key and the upload key. You keep the upload key and use it to sign your app for upload to the Google Play Store.
>
> https://developer.android.com/studio/publish/app-signing#google-play-app-signing

---

> IMPORTANT: Google will not re-sign any of your existing or new APKs that are signed with the app signing key. This enables you to start testing your app bundle in the internal test, alpha, or beta tracks while you continue to release your existing APK in production without Google Play changing it.
>
> https://play.google.com/apps/publish/?account=xxx#KeyManagementPlace:p=im.gitter.gitter&appid=xxx

---

We store the `.jks` keystore file as secret project variable(`signing_jks_file_hex`) in hex because it is a binary file. This hex gets piped back to a binary file in CI.

To generate the hex, you can use the following command,

```sh
xxd -p gitter-android-app.jks
```

## FAQ

### `Emulator: emulator: ERROR: x86 emulation currently requires hardware acceleration!`

If you have Hyper-V enabled on Windows 10, the Intel HAXM accelerator will refuse to install. See the section above for using the emulator with Hyper-V installed on Windows 10

Make sure you have the **Windows Hypervisor Platform** feature enabled in **Turn Windows features on or off**

### Ignore quick boot saved state (cold boot, restoring)

See https://medium.com/@jughosta/quick-boot-for-android-emulator-8224f8c4ea01

1.  **Tools** -> **AVD Manager**
1.  Use the dropdown arrow for the given virtual device -> **Cold Boot Now**

![](https://i.imgur.com/Xu9AfD5.png)
